const moment = require('moment');
const { INSTANCE, REPORT_SCHEDULE, MODE, FROM_DATE, TO_DATE } = require('../config.json');
const { getDatePayload } = require('./tools');

const settime = { h: 0, m: 0, s: 0, ms: 0 };
// const settime = { h: 23, m: 50, s: 0, ms: 0 };

let payload;

if (MODE === 'MANUAL') {
	payload = {
		from_date: encodeURIComponent(moment(new Date(FROM_DATE)).format('DD/MM/YYYY')),
		to_date: encodeURIComponent(moment(new Date(TO_DATE)).set(settime).format('DD/MM/YYYY')),
		from_timestamp: moment(new Date(FROM_DATE).getTime()).set(settime).unix(),
		to_timestamp: moment(new Date(TO_DATE).getTime()).set(settime).unix(),
	};
} else {
	// payload = {
	// 	from_date: encodeURIComponent(moment().set(settime).subtract(2, REPORT_SCHEDULE).format("DD/MM/YYYY")),
	// 	to_date: encodeURIComponent(moment().set(settime).format("DD/MM/YYYY")),
	// 	from_timestamp: moment().set(settime).subtract(2, REPORT_SCHEDULE).unix(),
	// 	to_timestamp: moment().set(settime).unix()
	// };

	let { from_date, to_date, from_timestamp, to_timestamp } = getDatePayload();
	payload = {
		from_date: encodeURIComponent(from_date),
		to_date: encodeURIComponent(to_date),
		from_timestamp,
		to_timestamp,
	};
}

module.exports = {
	getAPIs: function (botId) {
		return {
			agent_details: `${INSTANCE}/api/v1/agents/${botId}.json`,
			agent_usage: `${INSTANCE}/bots/analytics/${botId}/usage.json?from_date=${payload.from_date}&to_date=${payload.to_date}`,
			channel_usage: `${INSTANCE}/bots/analytics/${botId}/channel_usage.json?from_date=${payload.from_date}&to_date=${payload.to_date}`,
			top_intents: `${INSTANCE}/bots/analytics/${botId}/top_intents.json?from_date=${payload.from_date}&to_date=${payload.to_date}`,
			unhandled_messages: `${INSTANCE}/bots/analytics/${botId}/unhandled_messages.json?from_timestamp=${payload.from_timestamp}&to_timestamp=${payload.to_timestamp}`,
			unhandled_queries: `${INSTANCE}/api/v1/agents/${botId}/unhandled_queries.json?start_time=${payload.from_timestamp}&end_time=${payload.to_timestamp}`,
			successful_messages: `${INSTANCE}/bots/analytics/${botId}/successful_responses.json?from_timestamp=${payload.from_timestamp}&to_timestamp=${payload.to_timestamp}`,
			messages: `${INSTANCE}/bots/analytics/${botId}/messages.json?from_date=${payload.from_date}&to_date=${payload.to_date}`,
			query_insights: `${INSTANCE}/api/v1/agents/${botId}/query_insights.json?start_time=${payload.from_timestamp}&end_time=${payload.to_timestamp}`,
			top_qa_intents: `${INSTANCE}/bots/analytics/${botId}/top_knowldge_pack_intents.json?from_date=${payload.from_date}&to_date=${payload.to_date}`,
			top_feedback_intents: `${INSTANCE}/bots/analytics/${botId}/top_feedback_intents.json?from_date=${payload.from_date}&to_date=${payload.to_date}`,
			storages: `${INSTANCE}/dashboard/bots/${botId}/storages.json?order=&key=LIVEAGENTDATA`,
			feedbacks: `${INSTANCE}/dashboard/bots/${botId}/feedbacks.json?since_timetoken=${payload.from_timestamp}&timetoken=${payload.to_timestamp}&response_type=detailed`,
			user_feedback: `${INSTANCE}/dashboard/bots/${botId}/feedbacks/user_feedbacks.json?from_date=${payload.from_date}&to_date=${payload.to_date}`,
		};
	},
};
