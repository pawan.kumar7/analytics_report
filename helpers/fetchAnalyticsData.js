const request = require('request');
const colors = require('colors');
const fetch = require('node-fetch');
const { process_data, logger } = require('./tools');
const moment = require('moment');

const log = new logger('./log/reporting.log');

const { INSTANCE, REPORT_SCHEDULE, MODE, FROM_DATE, TO_DATE } = require('../config.json');
const settime = { h: 0, m: 0, s: 0, ms: 0 };

// const settime = { h: 23, m: 50, s: 0, ms: 0 };
let payload = {
	from_date: moment().set(settime).subtract(2, REPORT_SCHEDULE).format('MM/DD/YYYY'),
	to_date: moment().set(settime).format('MM/DD/YYYY'),
	from_timestamp: moment().set(settime).subtract(2, REPORT_SCHEDULE).unix(),
	to_timestamp: moment().set(settime).unix(),
};

if (MODE === 'MANUAL') {
	payload = {
		from_date: moment(new Date(FROM_DATE)).format('MM/DD/YYYY'),
		to_date: moment(new Date(TO_DATE)).set(settime).format('MM/DD/YYYY'),
		from_timestamp: moment(new Date(FROM_DATE).getTime()).set(settime).unix(),
		to_timestamp: moment(new Date(TO_DATE).getTime()).set(settime).unix(),
	};
} else {
	// payload = {
	// 	from_date: moment().set(settime).subtract(2, REPORT_SCHEDULE).format("MM/DD/YYYY"),
	// 	to_date: moment().set(settime).format("MM/DD/YYYY"),
	// 	from_timestamp: moment().set(settime).subtract(2, REPORT_SCHEDULE).unix(),
	// 	to_timestamp: moment().set(settime).unix()
	// };

	if (moment().date() === 1) {
		let date = '15';
		let month = moment().subtract(1, 'month').month() + 1;
		month = month.length === 1 ? '0' + month : month;
		let year = moment().year();
		let endOfMonth = moment().subtract(1, 'month').endOf('month').date();

		payload = {
			from_date: `${month}/${date}/${year}`,
			to_date: `${month}/${endOfMonth}/${year}`,
			from_timestamp: moment(new Date(`${month}/${date}/${year}`))
				.set(settime)
				.unix(),
			to_timestamp: moment(new Date(`${month}/${endOfMonth}/${year}`))
				.set(settime)
				.unix(),
		};
	} else {
		let date = '01';
		let month = moment().month() + 1;
		month = month.length === 1 ? '0' + month : month;
		let year = moment().year();

		payload = {
			from_date: `${month}/${date}/${year}`,
			to_date: moment().set(settime).format('MM/DD/YYYY'),
			from_timestamp: moment(new Date(`${month}/${date}/${year}`)).unix(),
			to_timestamp: moment().set(settime).unix(),
		};
	}
}

const context = require('../config.json');

const com_apis = {
	// development: {
	// 	apiAccess: {
	// 		dotComApis: {
	// 			authorization:
	// 				'Basic MmFhODgxYzYtNjM3My00OGVhLWFhNGUtMDdmOTY5ZmIyOTJlOmVhN2QxZTNlLWU0NGMtNDg4Zi05NTYwLTZmMjI5ZDM0ODQyNQ==',
	// 			apiEndpoints: {
	// 				oAuth: 'https://' + context.APIGW + '/auth/oauth/v2/token?grant_type=[GRANT_TYPE]',
	// 				toolsList:
	// 					'https://' +
	// 					context.APIGW +
	// 					'/extranet/api/MyTools/GetMyToolsBySignumAndAccountId/[SIGNUM]/[ACCOUNT_ID]',
	// 			},
	// 			client_id: '2aa881c6-6373-48ea-aa4e-07f969fb292e',
	// 			client_secret: 'ea7d1e3e-e44c-488f-9560-6f229d348425',
	// 		},
	// 	},
	// },

	development: {
		apiAccess: {
			dotComApis: {
				authorization:
					'Basic MmFhODgxYzYtNjM3My00OGVhLWFhNGUtMDdmOTY5ZmIyOTJlOmVhN2QxZTNlLWU0NGMtNDg4Zi05NTYwLTZmMjI5ZDM0ODQyNQ==',
				apiEndpoints: {
					oAuth:
						'https://' +
						context.APIGW +
						'/invoke/pub.apigateway.oauth2/getAccessToken?grant_type=client_credentials&client_id=[CLIENTID]&client_secret=[CLIENTSECRET]',
					toolsList:
						'https://' +
						context.APIGW +
						'/extranet/api/MyTools/GetMyToolsBySignumAndAccountId/[SIGNUM]/[ACCOUNT_ID]',
				},
				client_id: '9ca8b35b-daf3-46ba-83ec-630a56869d7c', //"a9c29b35-113a-4301-9a89-9324fc0d29ec",
				client_secret: '48d371ee-a216-45eb-89e5-720809b7d068', // "bac20676-1676-44e2-9e2f-3726b3bdfe3b"
			},
		},
	},
	staging: {
		apiAccess: {
			dotComApis: {
				authorization:
					'Basic MmFhODgxYzYtNjM3My00OGVhLWFhNGUtMDdmOTY5ZmIyOTJlOmVhN2QxZTNlLWU0NGMtNDg4Zi05NTYwLTZmMjI5ZDM0ODQyNQ==',
				apiEndpoints: {
					oAuth:
						'https://' +
						context.APIGW +
						'/invoke/pub.apigateway.oauth2/getAccessToken?grant_type=client_credentials&client_id=[CLIENTID]&client_secret=[CLIENTSECRET]',
					toolsList:
						'https://' +
						context.APIGW +
						'/extranet/api/MyTools/GetMyToolsBySignumAndAccountId/[SIGNUM]/[ACCOUNT_ID]',
				},
				client_id: 'b96a5289-b41b-409a-ad3b-71dc70a990d8',
				client_secret: 'dcee032a-3dce-4e7e-863e-58bc18bc9f44',
			},
		},
	},
	production: {
		apiAccess: {
			dotComApis: {
				authorization:
					'Basic MmFhODgxYzYtNjM3My00OGVhLWFhNGUtMDdmOTY5ZmIyOTJlOmVhN2QxZTNlLWU0NGMtNDg4Zi05NTYwLTZmMjI5ZDM0ODQyNQ==',
				apiEndpoints: {
					oAuth:
						'https://' +
						context.APIGW +
						'/invoke/pub.apigateway.oauth2/getAccessToken?grant_type=client_credentials&client_id=[CLIENTID]&client_secret=[CLIENTSECRET]',
					toolsList:
						'https://' +
						context.APIGW +
						'/extranet/api/MyTools/GetMyToolsBySignumAndAccountId/[SIGNUM]/[ACCOUNT_ID]',
				},
				client_id: '9ca8b35b-daf3-46ba-83ec-630a56869d7c', //"a9c29b35-113a-4301-9a89-9324fc0d29ec",
				client_secret: '48d371ee-a216-45eb-89e5-720809b7d068', // "bac20676-1676-44e2-9e2f-3726b3bdfe3b"
			},
		},
	},
};

const requestAsync = function (option) {
	return new Promise((resolve, reject) => {
		request(option.option, (err, response, body) => {
			if (err) {
				log(`Fetching: ${option.api}`.red.bold.inverse, `${option.option.url}`.underline);
				return reject(err, response, body);
			}

			if (response.statusCode == 200) {
				try {
					log(`Fetching: ${option.api}`.green.bold, `${option.option.url}`.underline);
					resolve({
						api: option.api,
						data: JSON.parse(body),
					});
				} catch (error) {
					resolve({
						api: option.api,
						data: [],
					});
				}
			} else {
				log(`Fetching: ${option.api}`.red.bold.inverse, `${option.option.url}`.underline);
				resolve({
					api: option.api,
					data: [],
				});
			}
		});
	});
};

let count = 0;
const getAnalytics = async function (options) {
	//transform requests into Promises, await all
	let data;
	try {
		data = await Promise.all(options.map(requestAsync));
		log(`\n---Fetching Complete---\n\n`.cyan.inverse);
		let build_options = process_data(data, options);
		// log(build_options)
		let new_data;
		if (build_options.length > 0 && count < 1) {
			count += 1;
			log(`Fetching pending data`.cyan.bold, count);
			new_data = await getAnalytics(build_options);
			log(`Fetching pending data Complete\n`.green.bold.inverse);
		}
		data = new_data ? new_data : data;
		// log(data)
		let total_clicks = await getBotClicks();
		log('total_clicks: ' + JSON.stringify(total_clicks));
		data.push(total_clicks[0]);
		return data;
	} catch (err) {
		console.error(err);
	}
};

async function fetchBearerToken(refreshToken = null) {
	let oAuthEndpoint = com_apis[context.NODE_ENV].apiAccess.dotComApis.apiEndpoints.oAuth;
	oAuthEndpoint = oAuthEndpoint.replace('[CLIENTID]', com_apis[context.NODE_ENV].apiAccess.dotComApis.client_id);
	oAuthEndpoint = oAuthEndpoint.replace(
		'[CLIENTSECRET]',
		com_apis[context.NODE_ENV].apiAccess.dotComApis.client_secret
	);

	let apiResponseStatusCode = 200;

	const requestParameters = {
		method: 'GET',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
	};

	return await fetch(oAuthEndpoint, requestParameters)
		.then((response) => {
			log('>>>>response', response.status);
			apiResponseStatusCode = response.status;
			if (apiResponseStatusCode === 200) {
				return response.json();
			}
			return null;
		})
		.then((data) => {
			return data;
		})
		.catch((error) => {
			log('\n\nfetch token error\n', error);
			return null;
		});
}

const getBotClicks = async () => {
	let token = await fetchBearerToken();
	log('token: ' + token);
	if (token && token.access_token && token.token_type === 'Bearer') {
		token = token.access_token;
	}

	let options = [
		{
			api: 'getChatBotLog',
			option: {
				url: `https://${context.APIGW}/I210214/v1/EnetUserLog/GetChatbotLog`,
				method: 'POST',
				headers: {
					'content-type': 'application/json',
					Authorization: `Bearer ${token}`,
				},
				body: JSON.stringify({
					signum: '',
					startDate: payload.from_date,
					endDate: payload.to_date,
				}),
				// strictSSL: false
			},
		},
	];

	log(JSON.stringify(options), payload);

	let bot_clicks = await Promise.all(options.map(requestAsync));
	log(`Bot clicks ${JSON.stringify(bot_clicks)}`);
	return bot_clicks;
};

module.exports = {
	getAnalytics,
	getBotClicks,
};
