const moment = require('moment');
let config = require('../config.json');
const _ = require('lodash');

if (config.NODE_ENV === 'development') {
	config.MAILTO = ['pawan.kumar@avaamo.com'];
	config.BCC = [];
}

const pipe =
	(...fns) =>
	(x) =>
		fns.reduce((v, f) => f(v), x);

const checkLastWeekUsers = (user) => {
	let today = new Date(moment().format('YYYY-MM-DD'));
	let last15date = new Date(moment().subtract(15, 'days').format('YYYY-MM-DD'));
	let test_date = new Date(user.time);
	// console.log(`Today: ${today}, last15date: ${last15date}`, new Date(last15date))
	// console.log(test_date <= today && test_date >= last15date)
	return test_date <= today && test_date >= last15date;
};
const getAPIData = (type, data, getData = []) => {
	// console.log(getData, type)
	let entries =
		getData.length > 0
			? (() => {
					let obj = [data.filter((d) => d.api === type)[0].data];
					// if (type === "top_intents") { console.log(obj) }
					let temp = getData.reduce((value, entry) => value && value[entry], obj);
					// if (type === "top_intents") { console.log(temp) }
					return temp;
			  })()
			: data.filter((d) => d.api === type)[0].data;
	// if (type === "top_intents") { console.log(entries) }
	return entries || (entries && entries.length > 0) ? entries : !['top_intents'].includes(type) ? 0 : '';
};

const getIntentRows = ({ intents, type = 'intents', feedback = false } = {}) => {
	// console.log('intent rows...', intents);

	const execludeIntents = ['Main Menu', 'Answers Handler', 'CSR Options', 'Filter CSR'];

	if (type === 'intents' && intents && intents.length) {
		intents = intents.filter((v) => !execludeIntents.includes(v.intent_name));

		if (feedback) {
			return intents
				.map((v, i) => {
					return `
			<tr>
				<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:left;vertical-align:top;word-break:normal">
					${i + 1}
				</td>
				<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:left;vertical-align:top;word-break:normal">
					<span style="font-style:normal;text-decoration:none">
						${v.intent_name}
					</span>
				</td>
				<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:left;vertical-align:top;word-break:normal">
					Positive: ${v.positive ? v.positive : 0}, Negative: ${v.negative ? v.negative : 0}
				</td>
			</tr>`;
				})
				.join(' ');
		}

		return intents
			.map((v, i) => {
				return `
			<tr>
				<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:left;vertical-align:top;word-break:normal">
					${i + 1}
				</td>
				<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:left;vertical-align:top;word-break:normal">
					<span style="font-style:normal;text-decoration:none">
						${v.intent_name}
					</span>
				</td>
				<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:left;vertical-align:top;word-break:normal">
					${v.intent_count}
				</td>
			</tr>`;
			})
			.join(' ');
	} else if (type === 'feedbacks') {
		if (intents && intents.length) {
			return intents
				.map(
					(v, i) =>
						`<tr style="border-bottom:1px solid #ccc; padding-bottom:2px">
						<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:left;vertical-align:top;word-break:normal">
						${i + 1}
						</td>
						<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:left;vertical-align:top;word-break:normal">
							<span style="font-style:normal;text-decoration:none">
								${v.comment}
							</span>
						</td>
					</tr>`
				)
				.join('');
		}
		return '';
	} else {
		return `<td style="background-color:#fff;border-color:#ccc;border-style:solid;border-width:0px;color:#333;font-family:Arial, Helvetica, sans-serif !important;;font-size:12px;overflow:hidden;padding:3px 20px;text-align:center;vertical-align:top;word-break:normal" colspan="3">
					No Data Present
				</td>`;
	}
};

const process_data = (data, options) => {
	let build_options = [];

	let filter_data = data.filter((d) => {
		if (d.data.status) {
			return false;
		}

		if (d.data.entries) {
			return d.data.entries.length < d.data.total_entries ? true : false;
		} else if (d.data.queries) {
			return d.data.queries.length < d.data.total_entries ? true : false;
		} else {
			return false;
		}
	});

	// console.log(filter_data)

	if (filter_data.length > 0) {
		for (let i = 0; i < options.length; i++) {
			if (filter_data.find((v) => v.api === options[i].api)) {
				let total_entries = filter_data.filter((v) => v.api === options[i].api)[0].data.total_entries;
				build_options.push({
					api: options[i].api,
					option: {
						method: options[i].option.method,
						headers: options[i].option.headers,
						url: options[i].option.url + `&per_page=${total_entries}`,
						strictSSL: false,
					},
				});
			} else {
				build_options.push(options[i]);
			}
		}
	}
	return build_options;
};

const getCSV = (json, fields = []) => {
	// console.log(json);

	if (!json || (json && json.length === 0) || (json && json === '')) return 'No Data';

	const { Parser } = require('json2csv');

	fields = fields || [];
	const opts = { fields };

	try {
		const json2csvParser = fields.length ? new Parser(opts) : new Parser();
		const csv = json2csvParser.parse(json);
		// console.log(csv);
		return csv;
	} catch (error) {
		console.log('CSV Parser Error:>', error);
		return null;
	}
};

const formatDate = (date) => {
	date = /[/]/gm.test(date) ? date.split('/') : date.split('-');
	return `${date[2]}-${date[1]}-${date[0]}`;
};

// Group Json date by conversationId using lodash groupBy function
const groupByConversationId = (data) => {
	return _.groupBy(data, (item) => item.Conversation_Id);
};

const fieldsCb = [{ type: 'date', cb: formatDate }];
const findFields = (fields, field) => fields.find((f) => f.type === field);

const formatData = (data) => {
	return data.map((item) => {
		let obj = { ...item };
		Object.keys(item).forEach((key) => {
			let field = findFields(fieldsCb, key.toLowerCase());
			if (field) {
				obj[key] = field.cb(item[key]);
			} else {
				obj[key] = item[key];
			}
		});
		return obj;
	});
};

const mailConfig = (emailHtml, analytics) => {
	let schedule = config.REPORT_SCHEDULE;
	if (schedule === 'days') {
		schedule = 'Daily';
	} else if (schedule === 'week') {
		schedule = 'Weekly';
	} else if (schedule === 'months') {
		schedule = 'Monthly';
	}

	let msg;
	if (Object.keys(analytics).length > 0) {
		let {
			bot,
			duration,
			total_queries,
			successful_queries,
			unhandled_queries,
			disambiguation,
			success_rate,
			agent_transfers,
			top_dialog_skills,
			feedbacks,
			csv_attachments,
		} = analytics;

		// console.log("feedbacks:", feedbacks);

		// console.log("csv:", csv_attachments);

		msg = {
			to:
				config.NODE_ENV === 'development'
					? ['pawan.kumar@avaamo.com', 'keerthi.k@ericsson.com']
					: config.MAILTO,
			from: 'no-reply@ericsson.com',
			bcc: config.NODE_ENV === 'development' ? [] : config.BCC,
			subject: `${bot} queries: ${total_queries}, unhandled: ${unhandled_queries}`,
			text: `${bot}: ${schedule} Usage Report`,
			html: emailHtml,
			attachments: csv_attachments
				? [
						{
							content:
								config.NODE_ENV === 'development'
									? Buffer.from(csv_attachments).toString('base64')
									: csv_attachments.toString('base64'),
							filename: 'Negative Feedback.csv',
							type: 'text/csv',
							disposition: 'attachment',
						},
				  ]
				: [],
		};
	} else {
		msg = {
			to: config.MAILTO,
			from: 'no-reply@ericsson.com',
			subject: `Agent Report Failed! Some Technical Error Occurred!`,
			html: emailHtml,
		};
	}
	return msg;
};

function logger(filePath) {
	this.filePath = filePath;
	const fs = require('fs');
	const util = require('util');
	const logFile = fs.createWriteStream(this.filePath, { flags: 'a' });
	const logStdout = process.stdout;

	return (...args) => {
		logFile.write(`[${moment().format('DD-MM-YYYY HH:mm:ss Z')}] => ` + util.format(...args) + '\n');
		logStdout.write(`[${moment().format('DD-MM-YYYY HH:mm:ss Z')}] => ` + util.format(...args) + '\n');
	};
}

const getDatePayload = (format = 'dd/mm/yyyy') => {
	const set_time = { h: 0, m: 0, s: 0, ms: 0 };
	let payload;
	if (moment().date() === 1) {
		let date = '15';
		let month = moment().subtract(1, 'month').month() + 1;
		month = month < 10 ? '0' + month : month;
		let year = moment().year();
		let endOfMonth = moment().subtract(1, 'month').endOf('month').date();

		payload = {
			from_date: format === 'mm/dd/yyyy' ? `${month}/${date}/${year}` : `${date}/${month}/${year}`,
			to_date: format === 'mm/dd/yyyy' ? `${month}/${endOfMonth}/${year}` : `${endOfMonth}/${month}/${year}`,
			from_timestamp: moment(new Date(`${month}/${date}/${year}`))
				.set(set_time)
				.unix(),
			to_timestamp: moment(new Date(`${month}/${endOfMonth}/${year}`))
				.set(set_time)
				.unix(),
		};
	} else {
		let date = '01';
		let month = moment().month() + 1;
		month = month < 10 ? '0' + month : month;
		let year = moment().year();

		payload = {
			from_date: format === 'mm/dd/yyyy' ? `${month}/${date}/${year}` : `${date}/${month}/${year}`,
			to_date:
				format === 'mm/dd/yyyy'
					? moment().set(set_time).format('MM/DD/YYYY')
					: moment().set(set_time).format('DD/MM/YYYY'),
			from_timestamp: moment(new Date(`${month}/${date}/${year}`)).unix(),
			to_timestamp: moment().set(set_time).unix(),
		};
	}
	return payload;
};

module.exports = {
	checkLastWeekUsers,
	getAPIData,
	getIntentRows,
	process_data,
	mailConfig,
	getCSV,
	pipe,
	formatData,
	groupByConversationId,
	getDatePayload,
	logger,
};
