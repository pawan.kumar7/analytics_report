const moment = require('moment');
var _ = require('lodash');
// var pdf = require('pdf-creator-node');
var fs = require('fs');
const { emailHtml } = require('./emailBody');
const { checkLastWeekUsers, getAPIData, getIntentRows, process_data, getCSV, getDatePayload } = require('./tools');
const { INSTANCE, REPORT_SCHEDULE, MODE, FROM_DATE, TO_DATE } = require('../config.json');
const Chart = require('../lib/Chart');

let payload;
const settime = { h: 0, m: 0, s: 0, ms: 0 };

payload = getDatePayload('mm/dd/yyyy');

const summary_data = (data) => {
	// const settime = { h: 23, m: 50, s: 0, ms: 0 };
	const settime = { h: 0, m: 0, s: 0, ms: 0 };

	// console.log(`Report Summary\n`.yellow.inverse);
	let bot = data.filter((d) => d.api == 'agent_details')[0].data.agent.display_name;
	let duration;
	if (MODE === 'MANUAL') {
		duration = `From ${moment(new Date(FROM_DATE)).set(settime).format('DD MMMM YYYY HH:MM')} TO ${moment(
			new Date(TO_DATE)
		)
			.set(settime)
			.format('DD MMMM YYYY HH:MM')} GMT`;
	} else {
		duration = `From ${moment(new Date(payload.from_date)).format('DD MMMM YYYY HH:MM')} TO ${moment(
			new Date(payload.to_date)
		).format('DD MMMM YYYY HH:MM')} GMT`;
	}

	let total_queries = getAPIData('agent_usage', data, [0, 'aggregate_stats', 'messages_count']);
	console.log('total_queries: ' + total_queries);
	let disambiguation = getAPIData('query_insights', data, [0, 'queries']);
	disambiguation =
		disambiguation.length > 0
			? disambiguation.filter((v) => v.intent_name.toLowerCase() === 'disambiguation').length
			: 0;
	let unhandled_queries = getAPIData('agent_usage', data, [0, 'aggregate_stats', 'unhandled_messages_count']);
	// let successful_queries = getAPIData("successful_messages", data, [0, "total_entries"])
	let successful_queries = getAPIData('agent_usage', data, [0, 'aggregate_stats', 'successful_responses_count']);
	let success_rate = getAPIData('agent_usage', data, [0, 'aggregate_stats', 'success_rate']);
	let agent_transfers = getAPIData('agent_usage', data, [0, 'aggregate_stats', 'agent_transfers_count']);
	let top_dialog_skills = getAPIData('top_intents', data, [0, 'top_intents']);
	top_dialog_skills = top_dialog_skills.length > 0 ? top_dialog_skills.map((i) => i.intent_name).join(',') : 'NaN';
	let top_qa_intents = getAPIData('top_qa_intents', data, [0, 'top_intents']);
	let top_feedback_intents = getAPIData('top_feedback_intents', data, [0, 'top_intents']);

	// console.log(top_feedback_intents);

	let feedbacks = getAPIData('feedbacks', data, [0, 'feedbacks']);
	feedbacks = feedbacks.length > 0 ? feedbacks : [];
	// console.log("feedbacks>>", feedbacks);
	feedbacks = feedbacks
		.filter((feedback) => !feedback.positive && feedback.comment != null && feedback.comment != '')
		.map((feedback) => {
			return {
				Date: new Date(feedback.created_at * 1000).toJSON().split('T')[0],
				Time: new Date(feedback.created_at * 1000).toTimeString().split(' ')[0],
				Comment: feedback.comment.trim(),
			};
		});

	let csv_attachments = feedbacks.length > 0 ? getCSV(feedbacks) : '';

	let agent_storages = getAPIData('storages', data, [0, 'storage'])[0];
	// console.log('\nagent_storages>>', agent_storages, typeof agent_storages);

	let total_clicks = getAPIData('getChatBotLog', data, [0, 'count']);

	return {
		bot,
		duration,
		total_queries,
		successful_queries,
		unhandled_queries,
		disambiguation,
		success_rate,
		agent_transfers,
		top_dialog_skills,
		top_qa_intents,
		top_feedback_intents,
		agent_storages,
		feedbacks,
		csv_attachments,
		total_clicks,
	};
};

const genrateReportBody = (data) => {
	// console.log(data);

	let report_summary = summary_data(data);
	// console.log(report_summary);
	let {
		bot,
		duration,
		total_queries,
		successful_queries,
		unhandled_queries,
		disambiguation,
		success_rate,
		agent_transfers,
		top_dialog_skills,
		top_qa_intents,
		top_feedback_intents,
		agent_storages,
		feedbacks,
		total_clicks,
	} = report_summary;

	let labels,
		datapoints,
		channel_usage_chart,
		user_feedbacks_chart,
		users_count_chart,
		users_count,
		queries_count_chart,
		agent_transfers_chart,
		live_agent_usage_chart,
		top_intents;

	let live_agent_data = {};

	// Channel Chart
	let channel_usage_data = getAPIData('channel_usage', data);

	if (channel_usage_data && channel_usage_data.channel_usage.length) {
		// console.log('channel_usage', channel_usage_data);
		labels = channel_usage_data.channel_usage.map((v) => `${v.channel}(${v.count})`);
		datapoints = [{ data: channel_usage_data.channel_usage.map((v) => v.count) }];
		channel_usage_chart = Chart.getChartLink({
			type: 'doughnut',
			labels: labels,
			datapoints: datapoints,
		});
	} else {
		channel_usage_chart = Chart.getChartLink({
			type: 'doughnut',
			labels: [],
			datapoints: [],
		});
	}

	// User Feedback Count
	let user_feedback = getAPIData('feedbacks', data, [0, 'feedbacks']);

	// console.log("user_feedback>>", user_feedback);

	if (user_feedback && user_feedback.length) {
		pos_user_feedback = user_feedback.filter((feedback) => feedback.positive);
		neg_user_feedback = user_feedback.filter((feedback) => !feedback.positive);
		// console.log(user_feedback, pos_user_feedback, neg_user_feedback);
		labels = ['Positive', 'Negative'];
		user_feedbacks_chart = Chart.getChartLink({
			type: 'doughnut',
			labels: labels,
			datapoints:
				pos_user_feedback.length === 0 && neg_user_feedback.length === 0
					? []
					: [{ data: [pos_user_feedback.length, neg_user_feedback.length] }],
		});
	} else {
		user_feedbacks_chart = Chart.getChartLink({
			type: 'doughnut',
			labels: [],
			datapoints: [],
		});
	}

	// Users Count Charts
	let users = getAPIData('query_insights', data, [0, 'queries']);
	if (!users) {
		users_count = 0;
		users_count_chart = Chart.getChartLink({ type: 'line', labels: [], datapoints: [] }, (chart = 'users_count'));
	} else {
		let temp_Data = users.map((v) => {
			v.created_at = moment(v.created_at * 1000).format('YYYY-MM-DD');
			v.user = v.user.id;
			return v;
		});
		temp_Data = _.groupBy(temp_Data, 'created_at');
		// short by date
		temp_Data = Object.keys(temp_Data)
			.sort()
			.reduce((r, k) => ((r[k] = temp_Data[k]), r), {});
		let modifiedData = {};
		Object.keys(temp_Data).map((v) => {
			modifiedData[v] = _.groupBy(temp_Data[v], 'user');
		});
		// console.log(modifiedData)
		labels = Object.keys(modifiedData);
		datapoints = [{ data: Object.keys(modifiedData).map((v) => Object.keys(modifiedData[v]).length) }];

		users_count = datapoints[0].data.reduce((acc, v) => acc + v, 0);
		users_count_chart = Chart.getChartLink(
			{ type: 'line', labels: labels, datapoints: datapoints },
			(chart = 'users_count')
		);
	}

	// Queries Count Charts
	let queries_count = getAPIData('agent_usage', data, [0, 'stats']);
	if (!total_queries || total_queries.length <= 0) {
		queries_count_chart = Chart.getChartLink(
			{ type: 'line', labels: [], datapoints: [] },
			(chart = 'queries_chart')
		);
	} else {
		let temp_Data = queries_count.map((v) => {
			v.start_time = moment(v.start_time * 1000).format('YYYY-MM-DD');
			return v;
		});

		temp_Data = _.groupBy(temp_Data, 'start_time');
		labels = Object.keys(temp_Data);
		let successful_data = [];
		let unhandled_data = [];
		let disambiguation_data = [];

		Object.keys(temp_Data).map((v) => {
			temp_Data[v].map((d) => {
				successful_data.push(d.successful_responses_count);
				unhandled_data.push(d.unhandled_messages_count);
				disambiguation_data.push(d.ambiguous_messages_count);
			});
		});

		datapoints = [{ data: successful_data }, { data: disambiguation_data }, { data: unhandled_data }];

		queries_count_chart = Chart.getChartLink(
			{ type: 'line', labels: labels, datapoints: datapoints },
			(chart = 'queries_chart')
		);
	}

	if (!agent_storages || agent_storages.length === 0) {
		// agent_storages = JSON.parse(agent_storages);
		// console.log(agent_storages);
		live_agent_data.total_attempts = 0;
		live_agent_data.total_success = 0;
		live_agent_data.total_timeout = 0;
		live_agent_data.total_offhours = 0;

		agent_transfers_chart = Chart.getChartLink(
			{ type: 'line', labels: [], datapoints: [] },
			(chart = 'live_agent_chart')
		);
	} else {
		agent_storages = JSON.parse(agent_storages.value);
		// console.log("agent_storages>>>", agent_storages);

		let temp = {};
		Object.keys(agent_storages.details).forEach((user) => {
			temp[user] = agent_storages.details[user]
				.map((i) => {
					i.time = moment(i.time).format('YYYY-MM-DD');
					return i;
				})
				.filter((i) => checkLastWeekUsers(i));
		});
		agent_storages.details = temp;

		temp = [];
		Object.keys(agent_storages.details).map((i) => agent_storages.details[i].map((o) => temp.push(o)));
		agent_storages.details = temp;
		// console.log('agent_storages>>', JSON.stringify(agent_storages));

		live_agent_data.total_attempts = agent_storages.details.length;
		live_agent_data.total_success = agent_storages.details.filter((i) => i.status === 'success').length;
		live_agent_data.total_timeout = agent_storages.details.filter((i) => i.status === 'timeout').length;
		live_agent_data.total_offhours = agent_storages.details.filter((i) => i.status === 'offhours').length;
		live_agent_data.details = agent_storages.details;
		live_agent_data.la_usage = agent_storages.la_usage;

		// live_agent_data = {
		// 	total_attempts: 1,
		// 	total_success: 0,
		// 	total_timeout: 1,
		// 	total_offhours: 0,
		// 	details: [{ user: 'EATMTIH', time: 1617110736777 , LA_lang: ['English','German']}],
		// }

		if (live_agent_data.details.length > 0) {
			let temp_Data = live_agent_data.details.map((v) => {
				v.time = moment(v.time).format('YYYY-MM-DD');
				return v;
			});

			temp_Data = _.groupBy(temp_Data, 'time');
			// console.log(temp_Data);
			labels = Object.keys(temp_Data);
			let success_transfers_count = [];
			let timeout_transfers_count = [];
			let offhours_transfers_count = [];
			Object.keys(temp_Data).forEach((v) => {
				success_transfers_count.push(temp_Data[v].filter((i) => i.status === 'success').length);
				timeout_transfers_count.push(temp_Data[v].filter((i) => i.status === 'timeout').length);
				offhours_transfers_count.push(temp_Data[v].filter((i) => i.status === 'offhours').length);
			});

			// console.log(agent_transfers_count);

			datapoints = [
				{ data: success_transfers_count },
				{ data: offhours_transfers_count },
				{ data: timeout_transfers_count },
			];
			// console.log(datapoints);

			agent_transfers_chart = Chart.getChartLink(
				{ type: 'line', labels: labels, datapoints: datapoints },
				(chart = 'live_agent_chart')
			);
		} else {
			agent_transfers_chart = Chart.getChartLink(
				{ type: 'line', labels: [], datapoints: [] },
				(chart = 'live_agent_chart')
			);
		}

		// Live Agent Transfers Usage Chart

		// console.log('live_agent_data.la_usage: ', live_agent_data.la_usage);

		if (live_agent_data.la_usage && live_agent_data.la_usage.length > 0) {
			let temp_Data = live_agent_data.la_usage
				.map((v) => {
					v.time = moment(v.time).format('YYYY-MM-DD');
					return v;
				})
				.filter((i) => checkLastWeekUsers(i));

			if (temp_Data.length) {
				temp_Data = _.groupBy(temp_Data, 'lang');

				temp_Data = Object.keys(temp_Data).map((v) => {
					return {
						intent_name: v,
						intent_count: temp_Data[v].length,
					};
				});
				// console.log('🚀 ~ temp_Data=Object.keys ~ temp_Data', temp_Data);
				live_agent_usage_chart = getIntentRows({ intents: temp_Data });
			} else {
				live_agent_usage_chart = getIntentRows({ intents: [] });
			}
		} else {
			live_agent_usage_chart = getIntentRows({ intents: [] });
		}
	}

	// console.log('live_agent_usage_chart: ', live_agent_usage_chart);

	// Top 10 Intents
	top_intents = getAPIData('top_intents', data, [0, 'top_intents']);
	top_intents = getIntentRows({ intents: top_intents });

	// Top 10 Knowledge Intents
	top_qa_intents = getIntentRows({ intents: top_qa_intents });

	// Top 10 Feedback Intents
	top_feedback_intents = getIntentRows({ intents: top_feedback_intents, feedback: true });

	// feedbacks = feedbacks.filter((feedback) => !feedback.positive && feedback.comment != null && feedback.comment != "");
	// feedbacks = feedbacks.map((feedback) => feedback.comment).join(",");
	// feedbacks = getIntentRows({ intents: feedbacks, type: "feedbacks" });

	return emailHtml({
		bot,
		duration,
		total_queries,
		successful_queries,
		unhandled_queries,
		disambiguation,
		success_rate,
		agent_transfers,
		top_dialog_skills,
		channel_usage_chart,
		users_count_chart,
		users_count,
		queries_count_chart,
		agent_transfers_chart,
		live_agent_usage_chart,
		top_intents,
		top_qa_intents,
		top_feedback_intents,
		live_agent_data,
		feedbacks,
		user_feedbacks_chart,
		total_clicks,
	});
};

module.exports = {
	summary_data,
	genrateReportBody,
};
