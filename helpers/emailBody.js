const config = require('../config.json');
const emailHtml = ({
	bot,
	duration,
	total_queries,
	successful_queries,
	unhandled_queries,
	disambiguation,
	success_rate,
	agent_transfers,
	top_dialog_skills,
	channel_usage_chart,
	users_count_chart,
	users_count,
	queries_count_chart,
	agent_transfers_chart,
	live_agent_usage_chart,
	top_intents,
	top_qa_intents,
	top_feedback_intents,
	live_agent_data,
	feedbacks,
	user_feedbacks_chart,
	total_clicks,
}) => {
	let schedule = config.REPORT_SCHEDULE;
	let NODE_ENV = config.NODE_ENV;
	if (schedule === 'days') schedule = 'Daily';
	if (schedule === 'week') schedule = 'Weekly';
	if (schedule === 'months') schedule = 'Monthly';

	total_clicks = total_clicks ? total_clicks : 0;

	let { total_attempts, total_offhours, total_timeout, total_success } = live_agent_data;

	// feedbacks = feedbacks.filter((feedback) => !feedback.positive && feedback.comment != null && feedback.comment != "");

	feedbacks =
		feedbacks.length > 0
			? `<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
												<div
													style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													"
												>
													<div
														class="txtTinyMce-wrapper"
														style="
															text-align: center;
															font-size: 11px;
															line-height: 1.2;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 17px;
														"
													>
														<p
															style="
																margin: 0;
																font-size: 11px;
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 17px;
																margin-top: 0;
																margin-bottom: 0;
															"
														>
															<strong>*Please Find the Attachment</strong><br>(Negative Feedback.csv)
														</p>
													</div>
												</div>
												<!--[if mso]></td></tr></table><![endif]-->`
			: '';

	return `<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office"
	xmlns:v="urn:schemas-microsoft-com:vml">

<head>
	<!--[if gte mso 9
			]><xml
				><o:OfficeDocumentSettings
					><o:AllowPNG /><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings
				></xml
			><!
		[endif]-->
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta content="width=device-width" name="viewport" />
	<!--[if !mso]><!-->
	<meta content="IE=edge" http-equiv="X-UA-Compatible" />
	<!--<![endif]-->
	<title></title>
	<!--[if !mso]><!-->
	<!--<![endif]-->
	<style type="text/css">
		body {
			margin: 0;
			padding: 0;
		}

		table,
		td,
		tr {
			vertical-align: top;
			border-collapse: collapse;
		}

		* {
			line-height: inherit;
		}

		a[x-apple-data-detectors='true'] {
			color: inherit !important;
			text-decoration: none !important;
		}
	</style>
	<style id="media-query" type="text/css">
		@media (max-width: 570px) {

			.block-grid,
			.col {
				min-width: 320px !important;
				max-width: 100% !important;
				display: block !important;
			}

			.block-grid {
				width: 100% !important;
			}

			.col {
				width: 100% !important;
			}

			.col_cont {
				margin: 0 auto;
			}

			img.fullwidth,
			img.fullwidthOnMobile {
				max-width: 100% !important;
			}

			.no-stack .col {
				min-width: 0 !important;
				display: table-cell !important;
			}

			.no-stack.two-up .col {
				width: 50% !important;
			}

			.no-stack .col.num2 {
				width: 16.6% !important;
			}

			.no-stack .col.num3 {
				width: 25% !important;
			}

			.no-stack .col.num4 {
				width: 33% !important;
			}

			.no-stack .col.num5 {
				width: 41.6% !important;
			}

			.no-stack .col.num6 {
				width: 50% !important;
			}

			.no-stack .col.num7 {
				width: 58.3% !important;
			}

			.no-stack .col.num8 {
				width: 66.6% !important;
			}

			.no-stack .col.num9 {
				width: 75% !important;
			}

			.no-stack .col.num10 {
				width: 83.3% !important;
			}

			.video-block {
				max-width: none !important;
			}

			.mobile_hide {
				min-height: 0px;
				max-height: 0px;
				max-width: 0px;
				display: none;
				overflow: hidden;
				font-size: 0px;
			}

			.desktop_hide {
				display: block !important;
				max-height: none !important;
			}
		}
	</style>
</head>

<body class="clean-body" style="margin: 0; padding: 0; -webkit-text-size-adjust: 100%; background-color: #ffffff">
	<!--[if IE]><div class="ie-browser"><![endif]-->
	<table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" class="nl-container" role="presentation" style="
				table-layout: fixed;
				vertical-align: top;
				min-width: 320px;
				border-spacing: 0;
				border-collapse: collapse;
				mso-table-lspace: 0pt;
				mso-table-rspace: 0pt;
				background-color: #ffffff;
				width: 100%;
			" valign="top" width="100%">
		<tbody>
			<tr style="vertical-align: top" valign="top">
				<td style="word-break: break-word; vertical-align: top" valign="top">
					<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color:#FFFFFF"><![endif]-->

					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:700px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="700" style="background-color:transparent;width:700px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;background-color:transparent;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											background-color: transparent;
											width: 700px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<div align="center" class="img-container center fixedwidth"
												style="padding-right: 0px; padding-left: 0px">
												<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><!
													[endif]--><img align="center" border="0" class="center fixedwidth"
													src="https://extranet-ericsson-assests.s3.amazonaws.com/exricsson_log_banner.png"
													style="
															text-decoration: none;
															-ms-interpolation-mode: bicubic;
															height: auto;
															border: 0;
															width: 100%;
															max-width: 700px;
															display: block;
														" width="700" />
												<!--[if mso]></td></tr></table><![endif]-->
											</div>
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 550px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																font-size: 14px;
																line-height: 1.2;
																word-break: break-word;
																text-align: center;
																mso-line-height-alt: 17px;
																margin: 0;
															">
														<strong><span style="font-size: 24px">${schedule} Usage
																Report</span></strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 550px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																font-size: 14px;
																line-height: 1.2;
																word-break: break-word;
																text-align: left;
																mso-line-height-alt: 17px;
																margin: 0;
															">
														<span style="font-size: 14px"><strong><span
																	style="">Summary</span></strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<div style="background-color: transparent">
						<div class="block-grid mixed-two-up no-stack" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 1px solid #A2A2A2; border-left: 1px solid #A2A2A2; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;background-color:#eeeeee;"><![endif]-->
								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #eeeeee;
											width: 182px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 1px solid #a2a2a2;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<strong>Bot Name</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="366" style="background-color:transparent;width:366px; border-top: 1px solid #A2A2A2; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;background-color:#eeeeee;"><![endif]-->
								<div class="col num8" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 360px;
											background-color: #eeeeee;
											width: 365px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 1px solid #a2a2a2;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<strong>${bot}</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<div style="background-color: transparent">
						<div class="block-grid mixed-two-up no-stack" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 0px solid transparent; border-left: 1px solid #A2A2A2; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;background-color:#eeeeee;"><![endif]-->
								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #eeeeee;
											width: 182px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 1px solid #a2a2a2;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<strong>Duration</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="366" style="background-color:transparent;width:366px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;background-color:#eeeeee;"><![endif]-->
								<div class="col num8" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 360px;
											background-color: #eeeeee;
											width: 365px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 1px solid #a2a2a2;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<strong>${duration}</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<!-- Total Queries -->
					<div style="background-color: transparent">
						<div class="block-grid mixed-two-up no-stack" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 0px solid transparent; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;background-color:#eeeeee;"><![endif]-->
								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #eeeeee;
											width: 182px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 1px solid #a2a2a2;
													border-bottom: 0px solid #a2a2a2;
													border-right: 0px solid transparent;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<strong>Total Queries</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="366" style="background-color:transparent;width:366px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;background-color:#eeeeee;"><![endif]-->
								<div class="col num8" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 360px;
											background-color: #eeeeee;
											width: 365px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<strong>${total_queries}</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<!-- Total Chat Cliks -->

					<div style="background-color: transparent">
						<div class="block-grid mixed-two-up no-stack" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 0px solid transparent; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;background-color:#eeeeee;"><![endif]-->
								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #eeeeee;
											width: 182px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 1px solid #a2a2a2;
													border-bottom: 1px solid #a2a2a2;
													border-right: 0px solid transparent;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<strong>Total Clicks</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="366" style="background-color:transparent;width:366px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px;background-color:#eeeeee;"><![endif]-->
								<div class="col num8" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 360px;
											background-color: #eeeeee;
											width: 365px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 1px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 0px;
													padding-bottom: 0px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<strong>${total_clicks}</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>


					<!-- QueriesCountChart -->

					<div style="background-color: transparent">
						<div class="block-grid two-up" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="275" style="background-color:transparent;width:275px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num6" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 270px;
											width: 275px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																font-size: 14px;
																mso-line-height-alt: 17px;
																margin: 0;
															">
														<span style="font-size: 14px"><strong>Queries</strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="275" style="background-color:transparent;width:275px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num6" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 270px;
											width: 275px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																text-align: right;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														Total Queries: ${total_queries}
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<!-- Quries Stats-->

					<div style="background-color: transparent">
						<div class="block-grid three-up" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 1px solid #A2A2A2; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;background-color:#f5f5f5;"><![endif]-->


								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #f5f5f5;
											width: 181px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 1px solid #a2a2a2;
													border-bottom: 1px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 0px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<span style="color: #3366ff"><strong><span
																	style="font-size: 16px">${successful_queries}</span></strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 10px; padding-top: 0px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<span style="color: #3366ff">Successful Queries</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 1px solid #A2A2A2; border-left: 0px solid transparent; border-bottom: 1px solid #A2A2A2; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;background-color:#f5f5f5;"><![endif]-->


								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #f5f5f5;
											width: 183px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 0px solid transparent;
													border-bottom: 1px solid #a2a2a2;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 0px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																font-size: 16px;
																mso-line-height-alt: 19px;
																margin: 0;
															">
														<span
															style="font-size: 16px; color: #00ff00">${disambiguation}</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 10px; padding-top: 0px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->


											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<span style="color: #00ff00">Disambiguation</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 1px solid #A2A2A2; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;background-color:#f5f5f5;"><![endif]-->


								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #f5f5f5;
											width: 181px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 1px solid #a2a2a2;
													border-bottom: 1px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 0px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																font-size: 16px;
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 19px;
																margin: 0;
															">
														<span
															style="font-size: 16px; color: #ff0000">${unhandled_queries}</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 10px; padding-top: 0px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<span style="color: #ff0000">Unhandled Queries</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<br />

					<!-- Quries Stats-->

					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 1px solid #A2A2A2; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 548px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 1px solid #a2a2a2;
													border-bottom: 1px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<div align="center"
												class="img-container center fullwidthOnMobile fixedwidth"
												style="padding-right: 0px; padding-left: 0px">
												<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><!
													[endif]--><img align="center" alt="Alternate text" border="0" class="center fullwidthOnMobile fixedwidth"
													src="${queries_count_chart}" style="
															text-decoration: none;
															-ms-interpolation-mode: bicubic;
															height: auto;
															border: 0;
															width: 100%;
															max-width: 90%;
															display: block;
														" title="Users Queries Count" width="438" />
												<!--[if mso]></td></tr></table><![endif]-->
											</div>
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<!-- Agent Intervention -->

					<div style="background-color: transparent">
						<div class="block-grid two-up" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="275" style="background-color:transparent;width:275px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num6" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 270px;
											width: 275px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																font-size: 14px;
																mso-line-height-alt: 17px;
																margin: 0;
															">
														<span style="font-size: 14px"><strong>Agent
																Intervention</strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="275" style="background-color:transparent;width:275px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num6" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 270px;
											width: 275px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																text-align: right;
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														Agent Transfers: ${total_attempts}
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<!--Agent Transfers Stats-->

					<div style="background-color: transparent">
						<div class="block-grid three-up" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 1px solid #A2A2A2; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;background-color:#f5f5f5;"><![endif]-->


								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #f5f5f5;
											width: 181px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 1px solid #a2a2a2;
													border-bottom: 1px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 0px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<span style="color: #3366ff"><strong><span
																	style="font-size: 16px">${total_success}</span></strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 10px; padding-top: 0px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->

											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<span style="color: #3366ff">Total Success</span>
													</p>
												</div>
											</div>

											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 1px solid #A2A2A2; border-left: 0px solid transparent; border-bottom: 1px solid #A2A2A2; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;background-color:#f5f5f5;"><![endif]-->


								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #f5f5f5;
											width: 183px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 0px solid transparent;
													border-bottom: 1px solid #a2a2a2;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 0px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																font-size: 16px;
																mso-line-height-alt: 19px;
																margin: 0;
															">
														<span
															style="font-size: 16px; color: #00ff00">${total_offhours}</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 10px; padding-top: 0px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->


											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<span style="color: #00ff00">Total Offhours</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="183" style="background-color:transparent;width:183px; border-top: 1px solid #A2A2A2; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;background-color:#f5f5f5;"><![endif]-->



								<div class="col num4" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 180px;
											background-color: #f5f5f5;
											width: 181px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 1px solid #a2a2a2;
													border-bottom: 1px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 0px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 0px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																font-size: 16px;
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 19px;
																margin: 0;
															">
														<span
															style="font-size: 16px; color: #ff0000">${total_timeout}</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 10px; padding-top: 0px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														<span style="color: #ff0000">Total Timeout</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->

							</div>
						</div>
					</div>

					</br>

					<!--Agent Transfers Chart-->

					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 1px solid #A2A2A2; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 548px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 1px solid #a2a2a2;
													border-bottom: 1px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<div align="center"
												class="img-container center fixedwidth fullwidthOnMobile"
												style="padding-right: 0px; padding-left: 0px">
												<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><!
													[endif]--><img align="center" alt="Alternate text" border="0" class="center fixedwidth fullwidthOnMobile"
													src="${agent_transfers_chart}" style="
															text-decoration: none;
															-ms-interpolation-mode: bicubic;
															height: auto;
															border: 0;
															width: 100%;
															max-width: 90%;
															display: block;
														" title="Agent Intervention Count" width="438" />
												<!--[if mso]></td></tr></table><![endif]-->
											</div>
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<div style="background-color: transparent">
						<div class="block-grid two-up" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="275" style="background-color:transparent;width:275px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num6" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 270px;
											width: 275px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																font-size: 14px;
																mso-line-height-alt: 17px;
																margin: 0;
															">
														<span style="font-size: 14px"><strong>Users</strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="275" style="background-color:transparent;width:275px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num6" style="
											display: table-cell;
											vertical-align: top;
											max-width: 320px;
											min-width: 270px;
											width: 275px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #000000;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #000000;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																text-align: right;
																mso-line-height-alt: 14px;
																margin: 0;
															">
														Count: ${users_count}
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 1px solid #A2A2A2; border-left: 1px solid #A2A2A2; border-bottom: 1px solid #A2A2A2; border-right: 1px solid #A2A2A2;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 548px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 1px solid #a2a2a2;
													border-left: 1px solid #a2a2a2;
													border-bottom: 1px solid #a2a2a2;
													border-right: 1px solid #a2a2a2;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<div align="center"
												class="img-container center fixedwidth fullwidthOnMobile"
												style="padding-right: 0px; padding-left: 0px">
												<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><!
													[endif]--><img align="center" alt="Alternate text" border="0" class="center fixedwidth fullwidthOnMobile"
													src="${users_count_chart}" style="
															text-decoration: none;
															-ms-interpolation-mode: bicubic;
															height: auto;
															border: 0;
															width: 100%;
															max-width: 90%;
															display: block;
														" title="Users Count Daily" width="438" />
												<!--[if mso]></td></tr></table><![endif]-->
											</div>
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<br>
					<div style="background-color: transparent">
						<div class="block-grid two-up" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div
								style="border-collapse: collapse; display: table; width: 100%; background-color: transparent">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:700px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="350" style="background-color:transparent;width:350px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num6"
									style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 348px; width: 350px">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div class="txtTinyMce-wrapper" style="
															text-align: center;
															font-size: 14px;
															line-height: 1.2;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 17px;
														">
													<p style="
																margin: 0;
																font-size: 14px;
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 17px;
																margin-top: 0;
																margin-bottom: 0;
															">
														<strong>Channel Usage</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<div align="center" class="img-container center autowidth"
												style="padding-right: 0px; padding-left: 0px">
												<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><!
													[endif]--><img align="center" border="0" class="center autowidth" src="${channel_usage_chart}" style="
															text-decoration: none;
															-ms-interpolation-mode: bicubic;
															height: auto;
															border: 0;
															width: 100%;
															max-width: 350px;
															display: block;
														" width="350" />
												<!--[if mso]></td></tr></table><![endif]-->
											</div>
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td><td align="center" width="350" style="background-color:transparent;width:350px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num6"
									style="display: table-cell; vertical-align: top; max-width: 320px; min-width: 348px; width: 350px">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div class="txtTinyMce-wrapper" style="
															text-align: center;
															font-size: 14px;
															line-height: 1.2;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 17px;
														">
													<p style="
																margin: 0;
																font-size: 14px;
																line-height: 1.2;
																word-break: break-word;
																mso-line-height-alt: 17px;
																margin-top: 0;
																margin-bottom: 0;
															">
														<strong>User Feedbacks</strong>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<div align="center" class="img-container center autowidth"
												style="padding-right: 0px; padding-left: 0px">
												<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr style="line-height:0px"><td style="padding-right: 0px;padding-left: 0px;" align="center"><!
													[endif]--><img align="center" border="0" class="center autowidth" src="${user_feedbacks_chart}" style="
															text-decoration: none;
															-ms-interpolation-mode: bicubic;
															height: auto;
															border: 0;
															width: 100%;
															max-width: 350px;
															display: block;
														" width="350" />
												<!--[if mso]></td></tr></table><![endif]-->
											</div>
											<!--[if (!mso)&(!IE)]><!-->
											${feedbacks}
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<br>

					<div style="background-color: transparent">
						<div class="block-grid" style="
																min-width: 320px;
																max-width: 700px;
																overflow-wrap: break-word;
																word-wrap: break-word;
																word-break: break-word;
																margin: 0 auto;
																background-color: transparent;
															">
							<div style="
																	border-collapse: collapse;
																	display: table;
																	width: 100%;
																	background-color: transparent;
																">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
																		min-width: 320px;
																		max-width: 700px;
																		display: table-cell;
																		vertical-align: top;
																		width: 550px;
																	">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
																				border-top: 0px solid transparent;
																				border-left: 0px solid transparent;
																				border-bottom: 0px solid transparent;
																				border-right: 0px solid transparent;
																				padding-top: 5px;
																				padding-bottom: 5px;
																				padding-right: 0px;
																				padding-left: 0px;
																			">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
																					color: #555555;
																					font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
																					line-height: 1.2;
																					padding-top: 10px;
																					padding-right: 10px;
																					padding-bottom: 10px;
																					padding-left: 10px;
																				">
												<div style="
																						line-height: 1.2;
																						font-size: 12px;
																						color: #555555;
																						font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
																						mso-line-height-alt: 14px;
																					">
													<p style="
																							line-height: 1.2;
																							word-break: break-word;
																							font-size: 14px;
																							mso-line-height-alt: 17px;
																							margin: 0;
																						">
														<span style="font-size: 14px"><strong>Top 10 Language
																Usage</strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<div style="background-color: transparent">
						<div class="block-grid" style="
																min-width: 320px;
																max-width: 700px;
																overflow-wrap: break-word;
																word-wrap: break-word;
																word-break: break-word;
																margin: 0 auto;
																background-color: transparent;
															">
							<div style="
																	border-collapse: collapse;
																	display: table;
																	width: 100%;
																	background-color: transparent;
																">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
																		min-width: 320px;
																		max-width: 700px;
																		display: table-cell;
																		vertical-align: top;
																		width: 550px;
																	">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
																				border-top: 0px solid transparent;
																				border-left: 0px solid transparent;
																				border-bottom: 0px solid transparent;
																				border-right: 0px solid transparent;
																				padding-top: 5px;
																				padding-bottom: 5px;
																				padding-right: 0px;
																				padding-left: 0px;
																			">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
																					color: #555555;
																					font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
																					line-height: 1.2;
																					padding-top: 0px;
																					padding-right: 0px;
																					padding-bottom: 0px;
																					padding-left: 0px;
																				">
												<div style="
																						line-height: 1.2;
																						font-size: 12px;
																						color: #555555;
																						font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
																						mso-line-height-alt: 14px;
																					">
													<table style="
																							width: 100%;
																							max-width: inherit;
																							border-collapse: collapse;
																							border-color: #ccc;
																							border-spacing: 0;
																							border-style: solid;
																							border-width: 1px;
																						" class="tg">
														<tbody>
															<tr style="background-color: #eeeeee">
																<td style="
																										border-color: #ccc;
																										border-style: solid;
																										border-width: 0px;
																										font-family: Arial, Helvetica, sans-serif !important;
																										font-size: 12px;
																										font-weight: bold;
																										overflow: hidden;
																										padding: 3px 20px;
																										text-align: left;
																										vertical-align: top;
																										word-break: normal;
																									">
																	NO
																</td>
																<td style="
																										border-color: #ccc;
																										border-style: solid;
																										border-width: 0px;
																										font-family: Arial, Helvetica, sans-serif !important;
																										font-size: 12px;
																										font-weight: bold;
																										overflow: hidden;
																										padding: 3px 20px;
																										text-align: left;
																										vertical-align: top;
																										word-break: normal;
																									">
																	LANGUAGE
																</td>
																<td style="
																										border-color: #ccc;
																										border-style: solid;
																										border-width: 0px;
																										font-family: Arial, Helvetica, sans-serif !important;
																										font-size: 12px;
																										font-weight: bold;
																										overflow: hidden;
																										padding: 3px 20px;
																										text-align: left;
																										vertical-align: top;
																										word-break: normal;
																									">
																	Count
																</td>
															</tr>
															${live_agent_usage_chart}
														</tbody>
													</table>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>


					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 550px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																font-size: 14px;
																mso-line-height-alt: 17px;
																margin: 0;
															">
														<span style="font-size: 14px"><strong>Top 10
																Intents</strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 550px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 0px;
														padding-left: 0px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<table style="
																width: 100%;
																max-width: inherit;
																border-collapse: collapse;
																border-color: #ccc;
																border-spacing: 0;
																border-style: solid;
																border-width: 1px;
															" class="tg">
														<tbody>
															<tr style="background-color: #eeeeee">
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	NO
																</td>
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	INTENTS
																</td>
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	Query Count
																</td>
															</tr>
															${top_intents}
														</tbody>
													</table>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 550px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																font-size: 14px;
																mso-line-height-alt: 17px;
																margin: 0;
															">
														<span style="font-size: 14px">
															<strong>Top 10 Knowledge Intents</strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 550px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 0px;
														padding-left: 0px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<table style="
																width: 100%;
																max-width: inherit;
																border-collapse: collapse;
																border-color: #ccc;
																border-spacing: 0;
																border-style: solid;
																border-width: 1px;
															" class="tg">
														<tbody>
															<tr style="background-color: #eeeeee">
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	NO
																</td>
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	INTENTS
																</td>
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	Query Count
																</td>
															</tr>
															${top_qa_intents}
														</tbody>
													</table>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 550px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																line-height: 1.2;
																word-break: break-word;
																font-size: 14px;
																mso-line-height-alt: 17px;
																margin: 0;
															">
														<span style="font-size: 14px">
															<strong>Top 10 Feedback Intents</strong></span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div style="
										border-collapse: collapse;
										display: table;
										width: 100%;
										background-color: transparent;
									">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:550px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="550" style="background-color:transparent;width:550px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12" style="
											min-width: 320px;
											max-width: 700px;
											display: table-cell;
											vertical-align: top;
											width: 550px;
										">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 0px;
														padding-right: 0px;
														padding-bottom: 0px;
														padding-left: 0px;
													">
												<div style="
															line-height: 1.2;
															font-size: 12px;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<table style="
																width: 100%;
																max-width: inherit;
																border-collapse: collapse;
																border-color: #ccc;
																border-spacing: 0;
																border-style: solid;
																border-width: 1px;
															" class="tg">
														<tbody>
															<tr style="background-color: #eeeeee">
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	NO
																</td>
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	INTENTS
																</td>
																<td style="
																			border-color: #ccc;
																			border-style: solid;
																			border-width: 0px;
																			font-family: Arial, Helvetica, sans-serif !important;
																			font-size: 12px;
																			font-weight: bold;
																			overflow: hidden;
																			padding: 3px 20px;
																			text-align: left;
																			vertical-align: top;
																			word-break: normal;
																		">
																	Query Count
																</td>
															</tr>
															${top_feedback_intents}
														</tbody>
													</table>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>

					<br /><br />
					<div style="background-color: transparent">
						<div class="block-grid" style="
									min-width: 320px;
									max-width: 700px;
									overflow-wrap: break-word;
									word-wrap: break-word;
									word-break: break-word;
									margin: 0 auto;
									background-color: transparent;
								">
							<div
								style="border-collapse: collapse; display: table; width: 100%; background-color: transparent">
								<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:700px"><tr class="layout-full-width" style="background-color:transparent"><![endif]-->
								<!--[if (mso)|(IE)]><td align="center" width="700" style="background-color:transparent;width:700px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px;"><![endif]-->
								<div class="col num12"
									style="min-width: 320px; max-width: 700px; display: table-cell; vertical-align: top; width: 700px">
									<div class="col_cont" style="width: 100% !important">
										<!--[if (!mso)&(!IE)]><!-->
										<div style="
													border-top: 0px solid transparent;
													border-left: 0px solid transparent;
													border-bottom: 0px solid transparent;
													border-right: 0px solid transparent;
													padding-top: 5px;
													padding-bottom: 5px;
													padding-right: 0px;
													padding-left: 0px;
												">
											<!--<![endif]-->
											<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
											<div style="
														color: #555555;
														font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
														line-height: 1.2;
														padding-top: 10px;
														padding-right: 10px;
														padding-bottom: 10px;
														padding-left: 10px;
													">
												<div class="txtTinyMce-wrapper" style="
															font-size: 12px;
															line-height: 1.2;
															color: #555555;
															font-family: Arial, Helvetica Neue, Helvetica, sans-serif;
															mso-line-height-alt: 14px;
														">
													<p style="
																margin: 0;
																text-align: right;
																line-height: 1.2;
																word-break: break-word;
																font-size: 13px;
																mso-line-height-alt: 16px;
																mso-ansi-font-size: 14px;
																margin-top: 0;
																margin-bottom: 0;
															">
														<span style="font-size: 13px; mso-ansi-font-size: 14px">Source :
															<strong>avaamo</strong> Conversational AI Platform</span>
													</p>
												</div>
											</div>
											<!--[if mso]></td></tr></table><![endif]-->
											<!--[if (!mso)&(!IE)]><!-->
										</div>
										<!--<![endif]-->
									</div>
								</div>
								<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
								<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
							</div>
						</div>
					</div>
					<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
				</td>
			</tr>
		</tbody>
	</table>
	<!--[if (IE)]></div><![endif]-->
</body>

</html>`;
};

module.exports = {
	emailHtml,
};
