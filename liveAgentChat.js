const fetch = require('node-fetch');
const https = require('https');
const csv = require('csvtojson');
var { emailHtml } = require('./helpers/liveChatTemplate');
const httpsAgent = new https.Agent({
	rejectUnauthorized: false,
});
const colors = require('colors');
const EmailHandler = require('./lib/EmailHandler');
const moment = require('moment');
let { INSTANCE, REPORT_SCHEDULE, ACCESS_TOKEN, AGENT_ID, NODE_ENV, LIVECHATNOTIFY, BCC } = require('./config.json');
const { getCSV, formatData, pipe, groupByConversationId, logger } = require('./helpers/tools');

const log = new logger('liveChat.log');

const { FetchData } = require('./lib/Fetch');

if (NODE_ENV == 'development') {
	LIVECHATNOTIFY = ['pawan.kumar@avaamo.com'];
	BCC = [];
}

const start_date = encodeURIComponent(moment().subtract(1, 'days').format('YYYY-MM-DD')) + 'T00:30:00.000Z';
const end_date = encodeURIComponent(moment().subtract(1, 'days').format('YYYY-MM-DD')) + 'T23:30:00.000Z';

const start_time = { h: 0, m: 0, s: 0, ms: 0 };
const end_time = { h: 23, m: 50, s: 0, ms: 0 };

const payload = {
	from_date: encodeURIComponent(moment().set(start_time).subtract(1, 'days').format('DD/MM/YYYY')),
	to_date: encodeURIComponent(moment().set(end_time).subtract(1, 'days').format('DD/MM/YYYY')),
	// from_timestamp: moment().set(start_time).subtract(1, "days").unix(),
	from_timestamp: moment().set(start_time).subtract(1, 'days').unix(),
	// to_timestamp: moment().subtract(1, "days").set(end_time).unix(),
	to_timestamp: moment().set(end_time).subtract(1, 'days').unix(),
};

const APIs = {
	transcript: `${INSTANCE}/dashboard/bots/${AGENT_ID}/agent_transcripts.csv?start_date=${start_date}&end_date=${end_date}`,
	LA_NOT_DATA: `${INSTANCE}/dashboard/bots/${AGENT_ID}/storages.json?order=&key=LA_NOT_DATA`,
	LA_TRANSCRIPT: `https://dev-ca-liveagent.chatbot.ericsson.net/api/conversations/exportChat?startDate=${start_date}&end_date=${end_date}`,
	feedbacks: `${INSTANCE}/dashboard/bots/${AGENT_ID}/feedbacks.json?since_timetoken=${payload.from_timestamp}&timetoken=${payload.to_timestamp}&response_type=detailed`,
	user_feedback: `${INSTANCE}/dashboard/bots/${AGENT_ID}/feedbacks/user_feedbacks.json?from_date=${payload.from_date}&to_date=${payload.to_date}`,
};

const option = {
	method: 'GET',
	headers: {
		'content-type': 'application/json',
		'access-token': ACCESS_TOKEN,
	},
	agent: httpsAgent,
};

const filterFromMonth = (data) =>
	data.filter((item) => {
		let D1 = new Date(moment().startOf('month').format('MM/DD/YYYY').toString());
		let D2 = new Date(moment().format('MM/DD/YYYY').toString());
		let D3 = new Date(moment(item.time_stamp).format('MM/DD/YYYY').toString());
		return D3 <= D2 && D3 >= D1;
	});

async function getLaNotData(url) {
	let json = await fetch(url, option)
		.then((response) => {
			if (response.status === 200) {
				log(`Fetching LA_NOT_DATA Successfully`.green);
				return response.json();
			}
			log(`Technical Error LA_NOT_DATA Fetching Failed`.yellow);
			return '';
		})
		.catch((err) => {
			log(`Fetch Error the Server: ${err}`.red);
			return '';
		});
	// log(json);
	return json && json.storage.length ? JSON.parse(json.storage[0].value) : null;
}

async function getTranscriptCSV(url) {
	return await fetch(url, {
		method: 'GET',
		headers: {
			'content-type': 'application/json',
			'access-token': ACCESS_TOKEN,
		},
		agent: httpsAgent,
	})
		.then((resp) => {
			if (resp.status === 200) {
				return resp.text();
			}
			return '';
		})
		.then((data) => {
			// log(data);
			if (!data) {
				log(`Technical Error CSV Fetching Failed`.yellow);
				return '';
			}

			log(`Fetching CSV Successfully`.green);
			return data;
		})
		.catch((err) => {
			log(`Fetch Error  the Server: ${err}`.red);
			return '';
		});
}

async function getTranscript() {
	log(APIs.LA_TRANSCRIPT);
	let msg = await getTranscriptCSV(APIs.LA_TRANSCRIPT);
	// log("msg", msg);

	// Chat Transcript CSV
	msg = await csv().fromString(msg);
	// log(msg);
	msg = pipe(
		formatData,
		groupByConversationId,
		(data) => {
			return Object.values(data).flat();
		},
		getCSV
	)(msg);
	// log(msg);

	// Live Agent User Info from Bot Storage
	let la_not_data = await getLaNotData(APIs.LA_NOT_DATA);
	// log(la_not_data);
	la_not_data = la_not_data
		? pipe(filterFromMonth, (la_not_data) => {
				return la_not_data.filter((f) => f.name != '');
		  })(la_not_data)
		: null;
	log('la_not_data : ', la_not_data?.length || null);
	const fields = ['name', 'signum', 'email', 'user_query', 'time_queried', 'reason'];
	la_not_data = getCSV(la_not_data, fields);

	// User Feedback CSV

	log(`Fetching User Feedback ${APIs.feedbacks}`);
	let feedbacks = await FetchData.getData(APIs.feedbacks, option);

	feedbacks = feedbacks && feedbacks.feedbacks && feedbacks.feedbacks.length > 0 ? feedbacks.feedbacks : [];
	feedbacks = pipe((feedbacks) => {
		return feedbacks
			.filter((feedback) => !feedback.positive && feedback.comment != null && feedback.comment != '')
			.map((feedback) => {
				return {
					Date: new Date(feedback.created_at * 1000).toJSON().split('T')[0],
					Time: new Date(feedback.created_at * 1000).toTimeString().split(' ')[0],
					Comment: feedback.comment.trim(),
				};
			});
	}, getCSV)(feedbacks);

	log(`Feedbacks CSV: ${feedbacks}`);

	let duration = `From ${moment()
		.startOf('month')
		.set({ h: 0, m: 30, s: 0, ms: 0 })
		.format('DD MMMM YYYY HH:MM')} TO ${moment()
		.subtract(1, 'days')
		.set({ h: 23, m: 50, s: 0, ms: 0 })
		.format('DD MMMM YYYY HH:MM')} GMT`;

	let ENV = NODE_ENV == 'staging' ? '(ACC)' : '';

	msg = {
		to: NODE_ENV === 'development' ? 'pawan.kumar@avaamo.com' : LIVECHATNOTIFY,
		from: NODE_ENV === 'development' ? 'pawan.kumar@avaamo.com' : 'no-reply@ericsson.com',
		bcc: BCC,
		subject: `Live Agent Chat Transcript ${ENV}`,
		html: emailHtml({ duration: duration }),
		attachments: [
			{
				content: NODE_ENV === 'development' ? Buffer.from(msg).toString('base64') : msg.toString('base64'),
				filename: 'Chat_Transcript.csv',
				type: 'text/csv',
				disposition: 'attachment',
			},
			{
				content:
					NODE_ENV === 'development'
						? Buffer.from(la_not_data).toString('base64')
						: la_not_data.toString('base64'),
				filename: 'Missed_Chat.csv',
				type: 'text/csv',
				disposition: 'attachment',
			},
		],
	};

	if (feedbacks.trim() != 'No Data') {
		msg.attachments.push({
			content:
				NODE_ENV === 'development' ? Buffer.from(feedbacks).toString('base64') : feedbacks.toString('base64'),
			filename: 'Negative_Feedbacks.csv',
			type: 'text/csv',
			disposition: 'attachment',
		});
	}

	let sendmail = new EmailHandler({ config: msg, type: 'sendGrid' });
	sendmail = await sendmail.mail();
	if (sendmail) {
		log(`\nMail sent Successfully\n`.green.inverse);
	} else {
		log(`\nTechnical error sending mail\n`.red.inverse);
	}
}

(async () => {
	log(`xxxxxxxx Live Agent Script Started xxxxxxxx`.green);
	log(`Live Agent Transcript Started at ${moment()}`.cyan);
	log(`From ${start_date} to ${end_date}\n`);
	await getTranscript();
	log(`======== Live Agent transcript Completed at ${moment()} ========\n\n`.green);
})();
