const colors = require('colors');
var fs = require('fs');
const moment = require('moment');
const { summary_data, genrateReportBody, process_data } = require('./helpers/utils');
const { getAnalytics } = require('./helpers/fetchAnalyticsData');
const config = require('./config.json');
const { getAPIs } = require('./helpers/analytics_api');
const EmailHandler = require('./lib/EmailHandler');
const { mailConfig, logger } = require('./helpers/tools');
const log = new logger('./log/reporting.log');

const genrateReport = async (email = '') => {
    const temp_opt = {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'access-token': config.ACCESS_TOKEN,
        },
        strictSSL: false,
    };

    let analytics_urls = getAPIs(config.AGENT_ID);
    let options = Object.entries(analytics_urls).map((api) => {
        let api_name = api[0];
        let api_url = api[1];
        // log(url)
        return {
            api: api_name,
            option: {
                ...temp_opt,
                url: api_url,
            },
        };
    });

    let msg, report_summary;

    let data = await getAnalytics(options);
    msg = genrateReportBody(data);
    report_summary = summary_data(data);

    let mail_config = mailConfig(msg, report_summary);

    let sendmail = new EmailHandler({ config: mail_config, type: 'sendGrid' });
    sendmail = await sendmail.mail();
    log('sendmail>>', sendmail);
    if (sendmail) {
        log(`\nMail sent Successfully\n`.green.inverse);
    } else {
        log(`\nTechnical error sending mail\n`.red.inverse);
    }
};

(async () => {
    log(`xxxxxxxx Agent Report Script Started xxxxxxxx`.green);
    var args = process.argv.slice(2);
    await genrateReport(args[0]);
    log(`======== Agent Report Completed ========`.green);
})();
