const fetch = require("node-fetch");

class FetchData {}
FetchData.getData = async (url, requestOptions) => {
	// console.log(url, requestOptions);
	return await fetch(url, requestOptions)
		.then(async (response) => {
			try {
				return response.json();
			} catch (error) {
				try {
					return response.text();
				} catch (error) {
					console.log("🚀 ~ .then ~ error", error);
				}
				console.log("🚀 ~ .then ~ error", error);
				return null;
			}
		})
		.then((data) => {
			if (!data) return null;
			// console.log(`\nFetched ${url}`);
			return data;
		})
		.catch((error) => {
			console.log(error);
			return null;
		});
};

const requestAsync = async (url, requestOptions) => {
	return new Promise((resolve, reject) => {
		fetch(url, requestOptions)
			.then((response) => {
				if (response.status === 200) return response.json();
				return null;
			})
			.then((data) => {
				if (!data) resolve("");
				// console.log(`Fetched ${url}`);

				resolve(data);
			})
			.catch((error) => {
				console.log(error);
				resolve("");
			});
	});
};

FetchData.getAsyncData = async (urls, requestOptions, { msg }) => {
	let response = await Promise.all(urls.map((url) => requestAsync(url, requestOptions)));
	return response.filter((item) => item != "");
};

module.exports = { FetchData };
