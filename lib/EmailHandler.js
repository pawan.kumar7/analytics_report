const sgMail = require('@sendgrid/mail');
const { NODE_ENV, SENDGRID_API_KEY } = require('../config.json');
const nodemailer = require('nodemailer');
sgMail.setApiKey(SENDGRID_API_KEY);

class EmailHandler {
	constructor({ config, type = 'smtp' }) {
		this.type = type;
		this.mailConfig = config;
	}
	async mail() {
		if (NODE_ENV === 'development') {
			this.mailConfig.from = 'no-reply@ericsson.com';
			// return await sgMail.send(this.mailConfig, (error, result) => {
			// 	if (error) {
			// 		// couldn't send email
			// 		//Do something with the error
			// 		console.log(error.code, error.response.body);
			// 		return false;
			// 	} else {
			// 		// console.log(result);
			// 		//email sent
			// 		return true;
			// 	}
			// });

			return new Promise((resolve, reject) => {
				let transport = nodemailer.createTransport({
					host: 'smtp-central.internal.ericsson.com',
					port: 25,
					secure: false,
					tls: { rejectUnauthorized: false },
				});

				transport.sendMail(this.mailConfig, function (error, info) {
					console.log('Sending mail...');
					if (error) {
						console.log('error is ' + error);
						resolve(false); // or use rejcet(false) but then you will have to handle errors
					} else {
						console.log('Email sent: ' + info.response);
						resolve(true);
					}
				});
			});
		} else {
			// send mail
			return new Promise((resolve, reject) => {
				let transport = nodemailer.createTransport({
					host: 'smtp-central.internal.ericsson.com',
					port: 25,
					secure: false,
					tls: { rejectUnauthorized: false },
				});

				transport.sendMail(this.mailConfig, function (error, info) {
					console.log('Sending mail...');
					if (error) {
						console.log('error is ' + error);
						resolve(false); // or use rejcet(false) but then you will have to handle errors
					} else {
						console.log('Email sent: ' + info.response);
						resolve(true);
					}
				});
			});
		}
	}
}

module.exports = EmailHandler;
