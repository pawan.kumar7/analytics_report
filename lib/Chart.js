class Chart {
	static getChartLink({ type, labels, datapoints }, chart = '') {
		const noData = `No Data Available`;

		const quickChartLink = `https://quickchart.io/chart?bkg=white&c=`;
		const colors = [
			'rgb(54, 162, 235)',
			'rgb(255, 205, 86)',
			'rgb(255, 99, 132)',
			'yellow',
			'purple',
			'magenta',
			'grey',
			'darkgreen',
			'pink',
			'brown',
			'slateblue',
			'orange',
			'gold',
		];

		const chartObj = {
			type: type,
			data: {
				labels: labels,
				datasets: datapoints,
			},
			options: {
				legend: {
					position: 'bottom',
					align: 'center',
					labels: {
						padding: 20,
					},
				},
			},
		};

		if (datapoints.length == 0) {
			chartObj.options.title = {
				display: true,
				text: noData,
			};

			return quickChartLink + encodeURIComponent(JSON.stringify(chartObj));
		}

		if (type === 'doughnut') {
			let props = [
				{
					backgroundColor: colors,
				},
			];
			let plugins = {
				legend: true,
				outlabels: {
					text: '%p',
					color: 'white',
					stretch: 30,
					font: {
						resizable: true,
						minSize: 11,
						maxSize: 18,
					},
				},
			};
			chartObj.options['cutoutPercentage'] = 75;
			// chartObj.options.plugins = plugins;
			// chartObj.options.legend.labels.padding = 50;
			chartObj.options.legend.align = 'center';
			chartObj.options.legend.labels.usePointStyle = true;

			chartObj.data.datasets.map((d, i) => Object.assign(chartObj.data.datasets[i], props[i]));
		}

		if (type === 'line') {
			let props = [
				{
					fill: true,
					borderColor: 'rgb(0, 128, 255)',
					lineTension: 0.0,
					borderWidth: 1.5,
					pointRadius: 0,
					backgroundColor: 'rgb(204, 229, 255)',
				},
				{
					fill: true,
					borderColor: 'rgb(51, 255, 153)',
					lineTension: 0.0,
					borderWidth: 1.5,
					pointRadius: 0,
					backgroundColor: 'rgb(153, 255, 204)',
				},
				{
					fill: true,
					borderColor: 'rgb(255, 51, 51)',
					lineTension: 0.0,
					borderWidth: 1.5,
					pointRadius: 0,
					backgroundColor: 'rgb(255, 153, 153)',
				},
			];
			chartObj.data.datasets.map((d, i) => Object.assign(chartObj.data.datasets[i], props[i]));

			let scales = {
				xAxes: [
					{
						ticks: { padding: 10 },
						gridLines: {
							drawTicks: false,
							z: 10,
						},
					},
				],
				yAxes: [
					{
						ticks: { padding: 10 },
						gridLines: {
							drawTicks: false,
							z: 10,
						},
					},
				],
			};

			let plugins = {
				filler: {
					propagate: true,
				},
			};

			chartObj.options.scales = scales;
			chartObj.options.plugins = plugins;
			chartObj.options.legend.display = false;
		}

		// console.log(`${quickChartLink + JSON.stringify(chartObj)}\n`.blue);
		return quickChartLink + encodeURIComponent(JSON.stringify(chartObj));
	}
}
module.exports = Chart;
